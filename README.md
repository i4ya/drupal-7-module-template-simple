CONTENTS
--------
 
* Item
 
 
ITEM
-----------------
 
* Good arguments for a TOC.  Makes sense, although I would add that a TOC should be optional for short READMEs that do not span over multiple pages (hard to measure, I know).
 
* I'd vote for a CVS Id standard in text files, too.  I've seen the following styles, but I still have in mind that text documentation files might be processed for HTML output (which is just a matter of time), so only the last of them would work out (under the assumption that CVS Ids should not be displayed in HTML output/API documentation).
 